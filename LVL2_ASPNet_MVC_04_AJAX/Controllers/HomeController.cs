﻿using LVL2_ASPNet_MVC_04_AJAX.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_04_AJAX.Controllers
{
    public class HomeController : Controller
    {
        DB_CUSTOMER_LUQMANEntities db = new DB_CUSTOMER_LUQMANEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult GetData(tbl_user user)
        {
            user = db.tbl_user.Where(x => x.id_user == 2).SingleOrDefault();
            return Json(user, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}